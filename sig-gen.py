import pydsdl
import sys


def print_type(field_type, indent=0):
    print(f"{' ' * indent * 4}{field_type}, {str(type(field_type))} Align {field_type.alignment_requirement}")


class SigBuilder(object):
    def __call__(self, s):
        self.sig += str(s)

    def __init__(self):
        self.sig = ""

    def get(self):
        return self.sig


ClassToCodeDict = {"VoidType": "v", "BooleanType": "b", "UnsignedIntegerType": "u", "SignedIntegerType": "i",
                   "FloatType": "f", "FixedLengthArrayType": "A", "VariableLengthArrayType": "a"}


def array_code(array):
    size = array.capacity
    class_name = array.__class__.__name__
    return f"{ClassToCodeDict[class_name]}{size}"


def primitive_code(primitive):
    bits = primitive.bit_length
    class_name = primitive.__class__.__name__
    return f"{ClassToCodeDict[class_name]}{bits}"


def process_node(c, dumper, accum, indent=0):
    if dumper is not None:
        dumper(c, indent)

    if isinstance(c, pydsdl.ServiceType):
        process_node(c.fields[0].data_type, dumper, accum, indent + 1)
        accum(';')
        process_node(c.fields[1].data_type, dumper, accum, indent + 1)
        return

    if (c.alignment_requirement > 1):
        accum('=' if c.alignment_requirement == 8 else f"=c.alignment_requirement")

    # https://uavcan.org/specification/UAVCAN_Specification_v1.0-beta.pdf#page=30
    if isinstance(c, pydsdl.CompositeType):

        if isinstance(c, pydsdl.DelimitedType):  # isinstance(c.inner_type, pydsdl.UnionType)
            delimiters = ("{", "}")
        elif isinstance(c, pydsdl.UnionType):
            delimiters = ("<", ">")
        elif isinstance(c, pydsdl.StructureType):
            delimiters = ("(", ")")
        else:
            raise Exception(f"{type(c)} signature not supported")

        accum(delimiters[0])
        for f in c.fields:
            process_node(f.data_type, dumper, accum, indent + 1)
        accum(delimiters[1])

    if isinstance(c, pydsdl.ArrayType):
        accum(array_code(c))
        accum('[')
        process_node(c.element_type, dumper, accum, indent + 1)
        accum(']')

    if isinstance(c, pydsdl.PrimitiveType) or isinstance(c, pydsdl.VoidType):
        accum(primitive_code(c))



def launch():
    ck = "C:\\Dev\\UavCan\\public_regulated_data_types\\reg\\"
    root = "C:\\Dev\\UavCan\\public_regulated_data_types\\uavcan\\"
    target_directory = root
    lookup_directories = root

    try:
        types = pydsdl.read_namespace(target_directory, lookup_directories)
    except pydsdl.InvalidDefinitionError as ex:
        print(f'{ex.path}:{ex.line}: Invalid DSDL: {ex.text}', file=sys.stderr)
        exit(1)
    else:
        msg = "node.GetInfo."
        # "uavcan.node.GetInfo.1.0": ".Value."
        for t in types:
            if msg in str(t):
                # print_type(t)
                accum = SigBuilder()
                process_node(t, print_type, accum, 0)  # replace None with print_type for detailed diagnostics
                print(accum.get())


if __name__ == '__main__':
    launch()
