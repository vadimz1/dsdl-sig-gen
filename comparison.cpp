#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdint.h>
#include <string.h>


int to_bytes(int bits)
{
    int bytes = (bits+7)/8;
    //printf("%d:%d\n", bits, bytes);
    return bytes;
}


int get_token(const char* &s) {

    int t = 0;
    if(isdigit(*s)) {
        do {
            //printf(">%c,%d\n", *s,t);
            t = t*10 + (*s-'0');
            s++;
        } while (isdigit(*s));
        //printf("t=%d\n", t);
        return -t; // numeric value - represent as negative
    }

    // not a number - use characted value
    return *s++;
}

int match_strict(const char* sig1, const char* sig2, bool debug = false)
{
    auto s1 = sig1, s2 = sig2;
    int prefix_bits, tail_bits;

    while(true) {

        auto c1=get_token(s1), c2=get_token(s2);

        if(debug)
            printf("tokens=%d %d\n", c1, c2);

        // both tokens are EOS = compatible
        if(c1 == '\0' && c2 == '\0') {
            return 1;
        }

        // only one is EOS = incompatible
        if(c1 == '\0' || c2 == '\0') {
            return 0;
        }

        // identical = move on
        if(c1 == c2) {

            // sealed types handling
            if(c1 == '(') {
                prefix_bits = tail_bits = 0;
            } else if (c1 == ')') {
                if (to_bytes(prefix_bits) != to_bytes(prefix_bits + tail_bits))
                    return 0; // incompatible if sealed types have different byte sizes
            } else if (c1 < 0) {
                prefix_bits -= c1;
            }

            continue;
        }

        // one is close bracket and other is not - advance the other (delimited)
        if(c1=='}') {
            s1--;
            continue;
        }

        if(c2=='}') {
            s2--;
            continue;
        }

        // one is close bracket and other is not - advance the other (sealed)
        if(c1=='|') {
            s1--;
            if (c2 < 0)  tail_bits -= c2;
            continue;
        }

        if(c2=='|') {
            s2--;
            if (c1 < 0)  tail_bits -= c1;
            continue;
        }

        // different tokens and not a special case
        return 0;

    } // while true

    return 0; //unreachable
}

void test_strict() {

    const char* tests[][3] = {
        {"1", "{u123{u8}a5f16i8}", "{u123{u8u16}a5f16}"},
        {"0", "(u123{u8}a5f16i8)", "(u123{u8u16}a5f16)"},
        {"1", "(u8u8b1)", "(u8u8b1b1u3)"},
        {"0", "(u8u8b1)", "(u8u8u3)"},
        {"0", "(u8u8b1)", "(u8u8b1u8)"},
        {"1", "", ""},
        };

    for(auto sigs : tests) {
        int r = match_strict(sigs[1], sigs[2]);
        int expected = atoi(sigs[0]);
        printf("'%s'\n'%s'\n %s %d\n\n",sigs[1], sigs[2], (r==expected)? "pass" : "fail", r);
    }
}


struct node {
    char token;
    uint64_t size;
};

bool equals(const node& n1, const node& n2) {
    return n1.token == n2.token && n1.size == n2.size;
}

node get_node(const char* &p) {

    node result = {0};

    result.token = *p++;

    if(isalpha(result.token)) {
        uint64_t t = 0;
        if(isdigit(*p)) {
            do {
                //printf(">%c,%d\n", *s,t);
                t = t*10 + (*p-'0');
                p++;
            } while (isdigit(*p));
        } else {
            t = -1;
        }
        //printf("t=%d\n", t);
        result.size = t;
    }

    return result;
}


bool is_array(char token) {
    return tolower(token) == 'a';
}

bool is_primitive_code(char token)
{   // data type and not array
    return !is_array(token) && isalpha(token);
}

// return either a real node, or synthetic void one, comprising of all empty space.
node get_node2(const char* &p, int& bits, bool debug=false) {
    uint void_bits = 0;
    node res = {0};
    auto s = p;
    bool tokenCaptured = false;

    while(true) {
        node n = get_node(s);

        if(n.token == 'v') {
            void_bits += n.size;
            res.token = 'v';
            res.size = void_bits;

            // consume and continue
            tokenCaptured = true;
            p = s;
            if(debug) printf("a %c %d vb=%d \n", n.token, n.size, void_bits);
            continue;
        }
        if(n.token == '=') {
            if(n.size == 0)
                n.size = 8;
            // calculate alignments and adjust void_bits
            if(bits > 0) {
                auto current_bits = bits + void_bits;
                auto current_bytes = to_bytes(current_bits); // TODO: support aligment to custom bitness from n.size
                auto aligned_bits = current_bytes*8;
                auto padding = aligned_bits - current_bits;
                if(debug) printf("N0 %d %d %d %d \n", current_bits, current_bytes, aligned_bits, padding);
                void_bits += padding;

                // save result
                if(padding > 0) {
                    res.token = 'v';
                    res.size = void_bits;
                    tokenCaptured = true;
                }
            } else {
                bits = 0; // restart tracking
            }


            // consume and continue
            p = s;
            if(debug) printf("N1%c %d vb=%d \n", n.token, n.size, void_bits);
            continue;
        }

        if(n.token == ')' || n.token == '(') {
            // consume and continue
            p = s;
            continue;
        }

        // consume first itentation, return without consuming otherwise
        if(!tokenCaptured) {
            res = n;
            p = s;
            tokenCaptured = true;
            if(debug) printf("d %c %d vb=%d \n", n.token, n.size, void_bits);
        }
        break;
    }

    return res;
}

//-------------------------------------------------------------------------------------
struct array {
    uint64_t size;
    int start_bits;
    bool fixed;
};

const int MAX_ARRAY_NESTING = 10;
// TODO: make statics stack vars and pass through, for reentrancy
static array array_stack[MAX_ARRAY_NESTING];
static int array_level = 0;

void push_array(const array& a) {
    if(array_level == MAX_ARRAY_NESTING-1)
        return; // TODO: error handling

    array_stack[++array_level] = a;
}

void pop_array(array& a) {
    if(array_level == 0)
        return; // TODO: error handling

    a = array_stack[array_level--];
}

//-------------------------------------------------------------------------------------


void update_position(const node& n, int& position_bits) {
    if(is_primitive_code(n.token) && position_bits>=0)
        position_bits += n.size;

    if(n.token == 'A') { // fixed length array
        array a;
        a.fixed = true;
        a.size = n.size;
        a.start_bits = position_bits;
        push_array(a);

        if(position_bits>=0) position_bits = 0;
    }

    if(n.token == 'a') { // variable length array
        array a;
        a.fixed = false;
        a.size = n.size;
        a.start_bits = position_bits;
        push_array(a);

        if(position_bits>=0) position_bits = 0;
    }

    if(n.token == ']') { // array closing

        array a;
        pop_array(a);
        if(a.fixed) {
            position_bits = position_bits * a.size + a.start_bits;
        } else {
            if(position_bits % 8 != 0)   // VL array of unaligned content
                position_bits = -1; // disable pos tracking and alignment from this point on
        }
    }

}

int match_lax(const char* sig1, const char* sig2, bool debug = false) {

    array_level = 0;

    int bits1=0, bits2=0;
    auto s1 = sig1, s2 = sig2;

    node c1, c2;
    bool c1_hold, c2_hold;
    while(true) {

        c1 = c1_hold ? c1 : get_node2(s1, bits1, debug);
        c2 = c2_hold ? c2 : get_node2(s2, bits2, debug);
        c1_hold = c2_hold = false;

        update_position(c1, bits1);
        update_position(c2, bits2);

        if(debug)
            printf("tokens %c%d %c%d (%d %d)\n", c1.token, c1.size, c2.token, c2.size, bits1, bits2);

        // both tokens are EOS = compatible
        /*
        if(c1.token == '\0' && c2.token == '\0') {
            return 1;
        }
        */

        // only one is EOS = compatible
        // 3.7.5.3 Top-level objects do not require the delimiter header
        // because the change in their length does not necessarily affect the backward compatibility
        if(c1.token == '\0' || c2.token == '\0') {
            return 1;
        }

        // identical = move on
        if( equals(c1, c2)) {
            continue;
        }

        // one is close bracket and other is not - advance the other (delimited)
        if(c1.token == '}') {
            c1_hold = true;
            continue;
        }

        if(c2.token == '}') {
            c2_hold = true;
            continue;
        }

        // padding matching
        if(c1.token == 'v' && is_primitive_code(c2.token) && c1.size >= c2.size) {
            c1.size -= c2.size;
            c1_hold = (c1.size > 0);    // hold if some padding remains
            continue;
        }

        if(c2.token == 'v' && is_primitive_code(c1.token) && c2.size >= c1.size) {
            c2.size -= c1.size;
            c2_hold = (c2.size > 0);    // hold if some padding remains
            continue;
        }

        // different tokens and not a special case
        if(debug)
            printf("mismatch %c%d %c%d (%d %d)\n", c1.token, c1.size, c2.token, c2.size, bits1, bits2);
        return 0;

    } // while true

    return -1; //unreachable
}

void test2() {

    auto sig = "={v2v3b1A5[u3]b1b1f16=a2[=(u7u7)]=(u7u7)u64A16[u8]a50[u7]=(u8u8)a1[u64]a222[u8]}";
    if(0) {
        auto s = sig;
        node n = {0};
        for(n = get_node(s); n.token != '\0'; n = get_node(s) ) {
            printf("%c %d\n", n.token, n.size);
        }
    }

    // string in "{v48v8b1(f16)a2[(u8u8)](u8u8)(u8u8)u64A16[u8]a50[u8]a1[u64]a222[u8]}";
    // tokens out "{ v56 b1 v7 f16 a2[ u8 u8 ]"

    // ={v48v8b1=(f16)=a2[=(u8u8)]=(u8u8)=(u8u8)u64A16[u8]a50[u8]a1[u64]a222[u8]}
    // ={v48v8b1f16=a2[=(u8u8)]=(u8u8)=(u8u8)u64A16[u8]a50[u8]a1[u64]a222[u8]}
    // ={v48v8b1f16A5[u3]=a2[=(u8u8)]=(u8u8)u64A16[u8]a50[u7]=(u8u8)a1[u64]a222[u8]}

}

void test_lax() {

    const char* tests[][3] = {
        {"1", "{u123{u8}a5f16i8}", "{u123{u8}a5f16i8}"},
        {"1", "{u123{u8}a5f16i8}", "{u123{u8u16}a5f16}"},
        {"1", "(u123{u8}a5f16i8)", "(u123{u8u16}a5f16)"},
        {"1", "(u8u8b1)", "(u8u8b1b1u3)"},
        {"0", "(u8u8b1)", "(u8u8u3)"},
        {"0", "(u8u8b1)", "(u8u8i1u8)"},
        {"1", "(u8v8b1)", "(u8u8b1)"},
        {"1", "(u8v8b1)", "(u8u4u4b1)"},
        {"1", "(u8u4=b1)", "(u8u4u4b1)"},
        {"1", "{u8u4u4b1}", "{u8u4=b1}"},
        {"1", "(u8u8b1)", "(u8(u8b1b1)u3)"},
        {"1", "u8A2[u2]", "u8A2[u2]"},
        {"1", "u8A2[A3[u2]]u4=b1", "u8A2[A3[u2]]=b1"},
        {"0", "u8A2[A3[u2]]u5=b1", "u8A2[A3[u2]]=b1"},
        {"1", "=(=(=(=(=(A3[f64]))=(A4[f32]))A21[f16])=(=(=(A3[f32])=(A3[f32]))A21[f16]))", "=(A3[f64])"},
        {"1", "", ""},
        };

    for(auto sigs : tests) {
        int r = match_lax(sigs[1], sigs[2]);
        int expected = atoi(sigs[0]);
        printf("'%s'\n'%s'\n %s res=%d\n\n",sigs[1], sigs[2], (r==expected)? "pass" : "fail", r);
        if(r!=expected) {
            puts("======= debug dump ==========");
            int rd = match_lax(sigs[1], sigs[2], true);   // debug dump on failure
            printf("res=%d\n", rd);
            puts("=============================");
        }
    }
}


int main() {

    test_lax();
    //test_strict

}
